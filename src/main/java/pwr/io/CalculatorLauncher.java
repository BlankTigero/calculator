package pwr.io;

import java.util.Scanner;

public class CalculatorLauncher {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println();
        int a = getNumberFromUser("Enter the first number: ");
        int b = getNumberFromUser("Enter the second number: ");
        String choice = getUserInput("What operation would you like to perform? (add, subtract, multiply, divide): ");

        double result = switch (choice) {
            case "add" -> Calculator.add(a, b);
            case "subtract" -> Calculator.subtract(a, b);
            case "multiply" -> Calculator.multiply(a, b);
            case "divide" -> Calculator.divide(a, b);
            default -> {
                System.out.println("Invalid operation. Try again.");
                yield Double.MIN_VALUE;
            }
        };

        if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) {
            System.out.println("You can't divide by zero.");
            main(args);
        }

        if (result != Double.MIN_VALUE) {
            System.out.println("The result is: " + result);
        }

        main(args);
    }

    private static String getUserInput(String prompt) {
        System.out.print(prompt);
        String userInput = scanner.nextLine();

        if ("".equals(userInput)) {
            return getUserInput(prompt);
        }

        return userInput;
    }

    private static int getNumberFromUser(String prompt) {
        System.out.print(prompt);
        boolean isInputInt = scanner.hasNextInt();
        int num;

        if (isInputInt) {
            num = scanner.nextInt();
            scanner.nextLine();
            return num;
        } else {
            System.out.println("\nYour input wasn't a number. Try again.");
            scanner.nextLine();
            return getNumberFromUser(prompt);
        }
    }
}
