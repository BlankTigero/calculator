package pwr.io;

public class Calculator {
    protected static int add(int a, int b) {
        return a + b;
    }

    protected static int subtract(int a, int b) {
        return a - b;
    }

    protected static int multiply(int a, int b) {
        return a * b;
    }

    protected static double divide(int a, int b) {
        return (double) a / b;
    }
}
